#include <iostream>
#include <string>
#include "graph.h"
using namespace std;

graph::~graph() {

}

void graph::convert(int **matrix,int width){

	  for (int i = 1; i <= width; i++) {
	  	addVertex(i);
	  }
	  
	  for (int i = 1; i <= width; i++) {
	    for (int j = 1; j <= width; j++) {
	    	if(matrix[i-1][j-1] > 0){ 
				//cout << "matrix " << i << " > " << j << endl;
				addEdge(i,j,matrix[i-1][j-1]); 
			}
		}
	  }	  
}

void graph::addVertex(int el) {
	Vertex *tmp,*tmp_2;
	tmp_2 = head;
	if (head == 0) {
		tmp = new Vertex(el,tail,0);
		tail = head = tmp;
	} else {
		tmp = tail;
		head->next = tmp;
		tail = new Vertex(el,0,0);
		tmp->next = tail;
	}
}

void graph::addEdge(int s,int d,int cost) {
	Vertex *tmp,*tmp2,*select;
	tmp = head;
	while (tmp != NULL) {
		if (tmp->Name() == s) {
			select = tmp;
			tmp2 = head;
			while (tmp2 != NULL) {
				if (tmp2->Name() == d) {
						select->addEdge(new Vertex(tmp2,0));
						this->is_weight = true;
					break;
				}  
				tmp2 = tmp2->next;   
			}
			break;
		}
		tmp = tmp->next;
	}
}

void graph::print() {
	Vertex *tmp, *select;
	tmp = head;
	int vertex_name,edge_name; 
	while (tmp != NULL) {
		select = tmp;
		vertex_name = tmp->Name(); 
		
		cout << "Vertex " << vertex_name << endl;
		cout << "Edge = ";
   
		while (select->edge != NULL) {
			select = select->edge;		
			edge_name = select->vertex->Name();		
			cout << vertex_name << " -> " << edge_name << "|";
		}
		
		cout << endl;
		
		tmp = tmp->next;
	}
}

bool graph::multi_check() {
  Vertex *tmp = head, *p, *chk;
  int counter = 0;
  while (tmp != NULL) {
   	if(tmp->edge == NULL){
   		return false;	
	}
    p = tmp->edge;
    chk = p->next;
    while (chk != NULL) {
      counter++;
      chk = chk->next;
      if (counter > 1) {
        return true;
      } else if (chk == NULL && p->edge != NULL) {
        p = p->edge;  
        chk = p->next;
        counter = 0;
      }
    }
    tmp = tmp->next;
  }
  return false;
}

bool graph::pseudo_check() {
  Vertex *tmp = head, *p;
  while (tmp != NULL) {
    p = tmp->edge;
    while (p != NULL) {
      if (p->Name() == tmp->Name()) {
        return true;
      }
      p = p->edge;
    }
    tmp = tmp->next;
  }
  return false;
}

bool graph::direct_check() {
  Vertex *tmp = head, *p, *chk;
 
    if(tmp->edge == NULL){
   		return false;	
	}
	
    p = tmp->edge;
    
    if (p->vertex->Name() != tmp->Name()) {		
    	chk = tail;
    	
   		while (chk->edge != NULL) {
			chk = chk->edge;		
			if(chk->vertex->Name() == tail->Name()){
				break;
			}
		}
      
      	cout << "ssdsd" << chk->vertex->Name(); 
      
        /*if (chk->vertex->Name() != p->vertex->Name()) {
        	 return true;
        }*/
		   
  	}
  	
  return false;
}

bool graph::weight_check() {
  return is_weight;
}

bool graph::complete_check() {
  Vertex *tmp = head, *p;
  int counter = 0,size = 0;
  
  while (tmp != NULL) {
  	size++;
    tmp = tmp->next; 
  }
  tmp = head;
  
  if(tmp->edge == NULL){
   		return false;	
  }
  
  while (tmp != NULL) {
  	
    p = tmp;
    while (p->edge != NULL) {
      counter++;
      p = p->edge;
      if (p->vertex->Name() == tmp->Name()) {
	  	break;
	  }
    }
    tmp = tmp->next;
  }
  
  if (counter == size*(size - 1))
    return true;
  else
    return false;
}
