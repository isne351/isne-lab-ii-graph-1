#ifndef Graph
#define Graph
#include <string>
using namespace std;


class Vertex {
public:
	Vertex *next;
	Vertex *edge;
	Vertex *vertex;
	Vertex() {
		this->next = this->edge  = 0;		
	}
	Vertex(Vertex *n,int cost) {
		this->vertex = n;
		this->cost = cost;
		this->edge = 0;
	}	
	Vertex(int el, Vertex *n = 0, Vertex *p = 0) {
		this->name = el;
		this->next = n;
		this->edge = 0;
	}
	void addEdge(Vertex *el){
		if(this->edge == 0){
			this->edge = el;
		}else{
			this->edge->addEdge(el);
		}
	}
	int Cost(){
		return this->cost;
	}
	int Name() {
		return this->name;
	}
private:
	int name;
	int cost;
};

class graph {
public:
	graph() {
		head = tail = 0;
	}
	~graph();
	bool isEmpty() { return head == 0; }
	void print();
	void convert(int **,int);
	bool complete_check();
	bool weight_check();
	bool direct_check();
	bool pseudo_check();
	bool multi_check();
private:
	void addVertex(int);
	void addEdge(int,int,int);	
	Vertex *head, *tail;
	bool is_weight = false;
};



#endif
